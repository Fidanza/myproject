/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "404.html",
    "revision": "5459e78bce1b7057a5ba424412994e0c"
  },
  {
    "url": "assets/css/0.styles.591bcb58.css",
    "revision": "2361243f06c629d15e429b1ffa751f58"
  },
  {
    "url": "assets/img/search.83621669.svg",
    "revision": "83621669651b9a3d4bf64d1a670ad856"
  },
  {
    "url": "assets/js/10.7342fa4a.js",
    "revision": "e38916cb372b7d98e35e5cfbe86cf834"
  },
  {
    "url": "assets/js/11.134eec3c.js",
    "revision": "f26ca7be32d85794292fd0bca28e0a1e"
  },
  {
    "url": "assets/js/12.3f60ff0f.js",
    "revision": "7745accf28f14f7e314692305a44c7cc"
  },
  {
    "url": "assets/js/13.f5f0c634.js",
    "revision": "ab8ca8cbce851f7b1e1cdeeee0fbe995"
  },
  {
    "url": "assets/js/14.22b989bc.js",
    "revision": "30aad5cd005ed67d8cbe1231825824a4"
  },
  {
    "url": "assets/js/15.9ee8b47b.js",
    "revision": "1de1ff50bc30d91b23dfaa75cfc93af4"
  },
  {
    "url": "assets/js/16.5e0c5837.js",
    "revision": "fb97979ba2960097b08e25eed239aaeb"
  },
  {
    "url": "assets/js/17.cd28510d.js",
    "revision": "4d3dd171bf7b87bdf3fece2b4b8182b1"
  },
  {
    "url": "assets/js/18.d94cf13e.js",
    "revision": "84a3f08bd3273caade0cb156cf1f1b2e"
  },
  {
    "url": "assets/js/19.8a371403.js",
    "revision": "4599dc48a20fabd3e8cd4ef4d3785ee0"
  },
  {
    "url": "assets/js/2.f8869771.js",
    "revision": "56313064af9ecf90a7d8b031325b06d9"
  },
  {
    "url": "assets/js/20.a2d5c916.js",
    "revision": "67556d66527a7a2a7bf94fe65ccbfbb1"
  },
  {
    "url": "assets/js/21.7546e642.js",
    "revision": "35a71ceb8d7b983c6b7421fb004c6ea3"
  },
  {
    "url": "assets/js/22.f83a8eea.js",
    "revision": "1ca06758dbe28e0484d6206f8b75b1d4"
  },
  {
    "url": "assets/js/23.aabe91d9.js",
    "revision": "f3ec4cb7834a05dc9de773f68e972743"
  },
  {
    "url": "assets/js/24.51459725.js",
    "revision": "b3d9e4af4f1c2741eff1c5e1eb79d7f5"
  },
  {
    "url": "assets/js/25.1b0cb678.js",
    "revision": "799931b93ccc2982665dc7b507d1ab5e"
  },
  {
    "url": "assets/js/26.5a9c1bbd.js",
    "revision": "98d25a28b36255acd9f6eadf0e3f3a78"
  },
  {
    "url": "assets/js/27.1118a3f1.js",
    "revision": "8e78cd28788ca7f158438d028a0f2c18"
  },
  {
    "url": "assets/js/28.60f51bd0.js",
    "revision": "0c28f65cd4ad2e03137f022dcb7adce3"
  },
  {
    "url": "assets/js/29.518d317c.js",
    "revision": "954695a13433b7bc16b902d92a6f72ca"
  },
  {
    "url": "assets/js/3.6ab6a856.js",
    "revision": "ae0234f27acdf1de5da0e4a8ed2d6ba4"
  },
  {
    "url": "assets/js/4.c8c4438e.js",
    "revision": "9d127f853b463fe4b8f280bb6caf97d0"
  },
  {
    "url": "assets/js/5.a85ab84d.js",
    "revision": "843d4381c85cdcaf856f63a739cdf3d4"
  },
  {
    "url": "assets/js/6.8364490f.js",
    "revision": "cfd4bd6b18199057a5034727ed98d27e"
  },
  {
    "url": "assets/js/7.afe76fdc.js",
    "revision": "da5d453b46857be8d01984ab62ac2425"
  },
  {
    "url": "assets/js/8.2331e688.js",
    "revision": "25f57a2079b9bb684c4c9b36b2637122"
  },
  {
    "url": "assets/js/9.90a82ed9.js",
    "revision": "f2451c0cd7fca1328ab1aac840e41f86"
  },
  {
    "url": "assets/js/app.a09db753.js",
    "revision": "9cdf7e8cac3c7b31b8e3d7b11ce94cbe"
  },
  {
    "url": "components/go-btn.html",
    "revision": "214a2533d0bbe4bbf89bf2f63f236694"
  },
  {
    "url": "components/go-readmore.html",
    "revision": "8efa9d4cde811763d381922e9085b2dc"
  },
  {
    "url": "components/go-route.html",
    "revision": "7d458fae45f07a56da6463562028890a"
  },
  {
    "url": "config.html",
    "revision": "2e65eead84a67d5b64e275c289728d6b"
  },
  {
    "url": "css-and-style/CSS - Fonts.html",
    "revision": "381d1b14da36079d9a8449c97e4e380a"
  },
  {
    "url": "css-and-style/CSS - Sizing.html",
    "revision": "edad85a47670c33c6ec2f28ed065bbcd"
  },
  {
    "url": "extensions/icon-genie.html",
    "revision": "3ade208eb88feebbb396622d0f138bbf"
  },
  {
    "url": "extensions/Introduction.html",
    "revision": "1ac36afacf659e90a69b34979ce53e3d"
  },
  {
    "url": "general/index.html",
    "revision": "7705043472c036e679ed883e62925a5a"
  },
  {
    "url": "hero.png",
    "revision": "980d95f59609acdb84ff66ed993f4721"
  },
  {
    "url": "icons/android-chrome-192x192.png",
    "revision": "f130a0b70e386170cf6f011c0ca8c4f4"
  },
  {
    "url": "icons/android-chrome-512x512.png",
    "revision": "0ff1bc4d14e5c9abcacba7c600d97814"
  },
  {
    "url": "icons/apple-touch-icon-120x120.png",
    "revision": "936d6e411cabd71f0e627011c3f18fe2"
  },
  {
    "url": "icons/apple-touch-icon-152x152.png",
    "revision": "1a034e64d80905128113e5272a5ab95e"
  },
  {
    "url": "icons/apple-touch-icon-180x180.png",
    "revision": "c43cd371a49ee4ca17ab3a60e72bdd51"
  },
  {
    "url": "icons/apple-touch-icon-60x60.png",
    "revision": "9a2b5c0f19de617685b7b5b42464e7db"
  },
  {
    "url": "icons/apple-touch-icon-76x76.png",
    "revision": "af28d69d59284dd202aa55e57227b11b"
  },
  {
    "url": "icons/apple-touch-icon.png",
    "revision": "66830ea6be8e7e94fb55df9f7b778f2e"
  },
  {
    "url": "icons/favicon-16x16.png",
    "revision": "4bb1a55479d61843b89a2fdafa7849b3"
  },
  {
    "url": "icons/favicon-32x32.png",
    "revision": "98b614336d9a12cb3f7bedb001da6fca"
  },
  {
    "url": "icons/msapplication-icon-144x144.png",
    "revision": "b89032a4a5a1879f30ba05a13947f26f"
  },
  {
    "url": "icons/mstile-150x150.png",
    "revision": "058a3335d15a3eb84e7ae3707ba09620"
  },
  {
    "url": "icons/safari-pinned-tab.svg",
    "revision": "f22d501a35a87d9f21701cb031f6ea17"
  },
  {
    "url": "index.html",
    "revision": "a8f745b29b858cfb9f52999071367d1d"
  },
  {
    "url": "introduction/directory-structure.html",
    "revision": "467826d78d30ab48337b8843487b1789"
  },
  {
    "url": "introduction/get-started.html",
    "revision": "44aee20fab857778ce094ba4a7240630"
  },
  {
    "url": "introduction/installation.html",
    "revision": "618a78d9925a90a354da8c08dc930c44"
  },
  {
    "url": "introduction/installing-and-uninstalling.html",
    "revision": "5474578624fcb40e52c7cd5b9e529f68"
  },
  {
    "url": "introduction/welcome.html",
    "revision": "7825a369ea66dbbdab07161a207d59a9"
  },
  {
    "url": "logo.png",
    "revision": "05dfdf7a57509b6197e129477f752a35"
  },
  {
    "url": "navlogo.png",
    "revision": "1254dd34545f8d7970c2d897ba1c83a5"
  },
  {
    "url": "others/index.html",
    "revision": "2f3f4e48f2729dd2ee67ee5103e95d0c"
  },
  {
    "url": "others/uninstalled.html",
    "revision": "e47b10c5490ea4ca6e18e1ff9879c96a"
  },
  {
    "url": "plugins/axios.html",
    "revision": "636d39533b0c3df81d829ec6cd5090f5"
  },
  {
    "url": "plugins/cordova.html",
    "revision": "9dfd0f9080c49a91352d6f972fdf277e"
  },
  {
    "url": "plugins/moment.html",
    "revision": "873d669bf0b13b825df06250b7df6171"
  },
  {
    "url": "plugins/vueawesomeswiper.html",
    "revision": "8033acadb7ceaad955207ec189478585"
  },
  {
    "url": "plugins/vuei18n.html",
    "revision": "8338e1327ce2c9b7eddcbb490504354e"
  },
  {
    "url": "vlogo.png",
    "revision": "cf23526f451784ff137f161b8fe18d5a"
  },
  {
    "url": "welcome.jpg",
    "revision": "78d2c82794e6eb3cbc882bab3066faf4"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
addEventListener('message', event => {
  const replyPort = event.ports[0]
  const message = event.data
  if (replyPort && message && message.type === 'skip-waiting') {
    event.waitUntil(
      self.skipWaiting().then(
        () => replyPort.postMessage({ error: null }),
        error => replyPort.postMessage({ error })
      )
    )
  }
})
