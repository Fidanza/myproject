module.exports = {
    title: 'GoBazar',
    description: 'Full frontend documentation',
    head: [
        ['link', { rel: 'icon', href: '/logo.png' }],
        // ['link', { rel: 'manifest', href: '/manifest.json' }],
        // ['meta', { name: 'theme-color', content: '#3eaf7c' }],
        // ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
        // ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
        // ['link', { rel: 'apple-touch-icon', href: '/icons/apple-touch-icon-152x152.png' }],
        // ['link', { rel: 'mask-icon', href: '/icons/safari-pinned-tab.svg', color: '#3eaf7c' }],
        // ['meta', { name: 'msapplication-TileImage', content: '/icons/msapplication-icon-144x144.png' }],
        // ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
    ],
    themeConfig: {
        logo: '/navlogo.png',
        // docsDir: 'docs',
        sidebar: [{
                title: 'Introduction',
                collapsable: false,
                children: [
                    '/introduction/welcome',
                    '/introduction/get-started',
                    '/introduction/installation',
                    '/introduction/directory-structure',
                    '/introduction/installing-and-uninstalling',
                ]
            },
            {
                title: 'Components',
                collapsable: false,
                children: [
                    '/components/go-btn',
                    '/components/go-readmore',
                    '/components/go-route',
                ]
            },
            {
                title: 'Plugins',
                collapsable: false,
                children: [
                    '/plugins/axios',
                    '/plugins/cordova',
                    '/plugins/moment',
                    '/plugins/vueawesomeswiper',
                    '/plugins/vuei18n',
                ]
            }
        ],
        smoothScroll: true,
        nextLinks: true,
        prevLinks: true,
        nav: [
            { text: 'Home Page', link: '/' },
            { text: 'Go Docs', link: '/introduction/welcome' },
        ],
    },
    plugins: [
        ['@vuepress/active-header-links'],
        ['@vuepress/nprogress'],
        ['@vuepress/medium-zoom'],
        ['@vuepress/back-to-top'],
        ['@vuepress/last-updated'],
        // Uncomment when publishing
        // ['@vuepress/pwa',
        //     {
        //         serviceWorker: true,
        //         updatePopup: true
        //     }
        // ]
    ]
}