---
title: Go button
---

# GoBtn custom component.

## What is GoBtn?

GoBtn is a custom and reusable button component created according to UX-Design of GoBazar. And it has left and right side icon modes. And to know more continue reading.

::: tip
Attributes we're using with components are called **prop attributes**. They help us to send and receive data between components or just to send data from the page to the component. We can call them as attributes too.
:::

## Tag look in HTML:
```html
<go-btn>Hello GoBazar!</go-btn>
```
## Attributes & props
```html
<go-btn
  type="text" text="Hello world" text-style="font-size: 120%"
  icon="wb-sunny" icon-side="left" icon-class="q-pt-md"
  icon-bg="red" property="q-mt-sm" @click="sayHi()"
  :block="false" :disabled="true"
/>
```

<!-- ```html
<go-btn
    type="text/submit"
    text="Hello world/Anything"
    text-style="font-size: 120%"
    icon="wb-sunny/fa fa-sun/any-icon"
    icon-side="left/right"
    icon-class="flip-horizontal/rotate-45/q-pt-md"
    icon-bg="red/blue/primary/yellow/#000000/var(--q-color-primary)"
    property="q-mt-sm/myheader/text-red/bg-blue-4/any-class"
    @click="$router.push('/your-account');sayHi()"
    :block="true/false"
    :disabled="true/false"
/>

``` -->



## Default values of attributes!

::: warning
There are such kind of attributes that they can work with its default property, even you don't write them. They have their own default values. You see them in the following list.
:::

- type="submit"
- icon="arrow_right_alt"
- icon-side="right"
- block="false"
- disabled="false"

## Attributes without default values
::: warning
 There are such kind of attributes can't work without specifying values. They are listed below. Of course, text, icon-bg should be filled in. Others are not required.
:::

- text=""
- text-style=""
- icon-class=""
- property=""
- icon-bg=""

## Boolean attributes

::: warning
If you use ``:block`` or ``:disabled`` mode, you should always write `:` before them. This is because the ``:block`` attribute only gets ``boolean``, namely, ``true/false ``. If you write without ``:`` it works, but there maybe errors in the ``console``.
:::

## Icon-side atribute
::: warning 
Don't forget the ``icon-side`` attribute gets only ``left`` or ``right`` not ``true/false``. If you write ``true/false`` you may get error or it may not work. This attribute don't get ``:`` before itself.
:::

## Colors:
To give default colors write their name in the property attibute. Like: ``property="primary"``. You can see other custom color names in the below list. And except them you can use the color name and color codes(**hex, rgb(a), hsl(a)**) but you shouldn't write custom color classes in the property attribute, but in the **btn-class** tag like: ``btn-class="color: red"`` or ``color: #000``.
```css
.primary {
  background: var(--q-color-primary);
}
.secondary {
  background: var(--q-color-secondary);
}
.dark {
  background: var(--q-color-dark);
}
.positive {
  background: var(--q-color-positive);
}
.negative {
  background: var(--q-color-negative);
}
.warning {
  background: var(--q-color-warning);
}
.accent {
  background: var(--q-color-accent);
}
.info {
  background: var(--q-color-info);
}
```
 
## Sizing
You can specify button sizes using classes in property attribute. They are listed below.

```css
.size-xs {}
.size-sm {}
.size-md {}
.size-lg {}
.size-xl {}
```


## Ready style classes:
These are helpful classess to help you to create the button more faster and they avoid writing many styles in the btn-style or icon-style attrubutes. Just use classes in the property tag. However, you want to specify your own properties use the btn-style and icon-style attributes.

```css
.text-bold {
  font-weight: bold;
}
.text-italic {
  font-style: italic;
}
.text-align-center {
  text-align: center;
}
.full {
    width: 100%;
}
```

## Examples:
### Primary(block="false", type="submit")
```html
<go-btn
   text="Hello GoBazar"
   type="submit"
   property="
   size-md
   primary
   text-bold
   text-align-center"
   icon-bg="#e53f2d"
   text-style="font-size: 16px"
></go-btn>
```
### Secondary(block="true", type="text")
```html
<go-btn
    text="Hi GoBazar"
    block="true"
    property="
    size-md
    secondary
    text-bold
    text-align-center"
    icon-bg="#232b38"
    text-style="font-size: 16px"
></go-btn>
```


### Warning(icon-side="left", block="false" )
::: tip
Using flip-horizontal or flip-vertical in ``icon-class`` attribute you can flip the icon. And when in the left side mode specify ``l-`` before sizing classes.
:::
```html
<go-btn
    text="Hey! GoBazar!"
    icon-side="left"
    property="
    l-size-md
    warning
    text-bold
    text-align-center"
    icon="arrow_right_alt"
    icon-bg="#f08d49"
    icon-class="flip-horizontal"
    text-style="font-size: 16px"
></go-btn>
```

### With onclick
::: tip
Onclick can be defined in many ways. So as ``onclick=""``, ``@click=""`` and ``v-on:click``. But we recommend using vue default and universal **@click**. Because it is well supported than others.
:::
<go-btn
    @click="someFunction"
    text="Go! Bazar!"
    property="
    size-md
    positive
    text-bold
    text-align-center"
    icon="arrow_right_alt"
    icon-bg="#7ec699"
    text-style="font-size: 16px"
></go-btn>


## Attributes in detail:
### type
Use ``type`` attribute to show if the button is form subitting button or not. In default it is not a submit button. If you want to use submit button, write ``type="submit"`` in the`` <go-btn />`` tag. But you have one another choice too. Which is just **button**. When you specify type as button like ``type="button"``, go-btn loses it form submit feature and become normal button.

### text
Write anything to show in the center of the button as a label. And you can display functional text by just adding : before text attribute.

### icon
This is the icon which is placed on the right(or maybe left) side of the button.
You can use ``material-icons/font-awesome-icons`` withtit. If you want to add another type of the button library check for the ``quasar.conf.js`` file. Find **framework>icons** part. And uncomment that icon-type if you want to use that icon library.

### property(equal to class)
Yeap, property prop is class attribute for GoBazar button. Just when propping we called it out so to be different than usual buttons and to show this is another kind of button than simple buttons. Write any class in it. Anything you can specify in classes can be specified here.

### icon-bg
Use this attribute to specify the icon background colour.

### @click/routing/functions
To perform actions like going somewhere, doing something or others we use ``@click``.
This is **the same** with defualt VueJS and Quasar's ``@click``. And even if you can use it the same way like ``v-on:click="somehting"``. What about functions. Even everything is the same. And this was to show how is our ``@click`` is the same with defaults. But what about ``href`` and ``to``? Yes they are available but inside ``@click``. Yes you understand correctly.

To go with ``href`` write urls in ``@click`` like this: ``window.open("https://www.w3schools.com");``. This opens a new window/tab. To open  it in this page simply use _slef after url address like this: ``window.open("https://www.w3schools.com", "_self");``.
For more information and options. Go with this [address](https://www.w3schools.com/jsref/met_win_open.asp).


Ok what about routing? This is too easy: in ``@click`` write: ``$router.push('/somewhere')``. Yes just it is. Replace /somewhere with route. That's it. Done.

### ``:block`` mode button
We speak about so many things about this in one of the above notes.
So what we can tell you here is: it is used to make button width the same with its container. Just block mode should always a boolean attribute. So that it gets true/false. If not specified it gets false. And before that attribute there should always be ``:``.

<script>
export default {
  methods: {
    someFunction() {

    }
  }
}
</script>