# ReadMe First!

## To see successfully installed project follow these steps in order.

### 1. After downloading and extracting, then enter root of the project using ``CMD/Terminal``
### 2. Then write
```css
yarn install
```
src
 ┣ app
 ┃ ┣ accounts
 ┃ ┃ ┣ auth
 ┃ ┃ ┃ ┣ login.vue
 ┃ ┃ ┃ ┣ password-change.vue
 ┃ ┃ ┃ ┗ register.vue
 ┃ ┃ ┣ components
 ┃ ┃ ┃ ┣ orders
 ┃ ┃ ┃ ┃ ┣ estimated-orders-container.vue
 ┃ ┃ ┃ ┃ ┗ usual-orders-container.vue
 ┃ ┃ ┃ ┣ payment
 ┃ ┃ ┃ ┃ ┣ default-payment-methods-container.vue
 ┃ ┃ ┃ ┃ ┗ payment-methods-container.vue
 ┃ ┃ ┃ ┣ address-box.vue
 ┃ ┃ ┃ ┣ NotUsed-login-2.vue
 ┃ ┃ ┃ ┗ orders-box.vue
 ┃ ┃ ┣ account-routes.js
 ┃ ┃ ┣ payment-methods.vue
 ┃ ┃ ┣ your-account.vue
 ┃ ┃ ┣ your-address-book.vue
 ┃ ┃ ┗ your-orders.vue
 ┃ ┣ billing
 ┃ ┃ ┣ carts
 ┃ ┃ ┃ ┣ cart-products.vue
 ┃ ┃ ┃ ┣ cart-sticky-products.vue
 ┃ ┃ ┃ ┣ cart-sticky.vue
 ┃ ┃ ┃ ┣ cart.vue
 ┃ ┃ ┃ ┗ mobile-cart-full.vue
 ┃ ┃ ┗ checkout
 ┃ ┃ ┃ ┣ checkout-main.vue
 ┃ ┃ ┃ ┣ sendContactForm.vue
 ┃ ┃ ┃ ┗ test-checkout.vue
 ┃ ┣ products
 ┃ ┃ ┣ categories
 ┃ ┃ ┃ ┗ category-list.vue
 ┃ ┃ ┣ product-details
 ┃ ┃ ┃ ┣ components
 ┃ ┃ ┃ ┃ ┣ reviews
 ┃ ┃ ┃ ┃ ┃ ┣ mobile-reviews.vue
 ┃ ┃ ┃ ┃ ┃ ┣ review-contents.vue
 ┃ ┃ ┃ ┃ ┃ ┣ reviews2.vue
 ┃ ┃ ┃ ┃ ┃ ┗ web-reviews.vue
 ┃ ┃ ┃ ┃ ┣ index.html
 ┃ ┃ ┃ ┃ ┣ nintendo.jpg
 ┃ ┃ ┃ ┃ ┣ product-detail-attributes.vue
 ┃ ┃ ┃ ┃ ┣ product-detail-description.vue
 ┃ ┃ ┃ ┃ ┣ product-detail-header.vue
 ┃ ┃ ┃ ┃ ┣ product-detail-images.vue
 ┃ ┃ ┃ ┃ ┣ rating_stars.vue
 ┃ ┃ ┃ ┃ ┣ read_more.vue
 ┃ ┃ ┃ ┃ ┗ reviews2.vue
 ┃ ┃ ┃ ┗ product-details.vue
 ┃ ┃ ┣ product-list
 ┃ ┃ ┃ ┣ components
 ┃ ┃ ┃ ┃ ┣ aspects
 ┃ ┃ ┃ ┃ ┃ ┣ aspect-options.vue
 ┃ ┃ ┃ ┃ ┃ ┗ aspects.vue
 ┃ ┃ ┃ ┃ ┣ category-list.vue
 ┃ ┃ ┃ ┃ ┣ child-category-list.vue
 ┃ ┃ ┃ ┃ ┣ filter-item-list.vue
 ┃ ┃ ┃ ┃ ┣ filter-item-range.vue
 ┃ ┃ ┃ ┃ ┣ product-card-grid.vue
 ┃ ┃ ┃ ┃ ┗ product-card-list.vue
 ┃ ┃ ┃ ┣ product-list-filter.vue
 ┃ ┃ ┃ ┣ product-list-header.vue
 ┃ ┃ ┃ ┗ product-list.vue
 ┃ ┃ ┣ review
 ┃ ┃ ┃ ┗ review.vue
 ┃ ┃ ┣ shared
 ┃ ┃ ┃ ┣ components
 ┃ ┃ ┃ ┃ ┣ dao-product.vue
 ┃ ┃ ┃ ┃ ┣ product-cards-gallery.vue
 ┃ ┃ ┃ ┃ ┣ product-list-trendingOtabekDeleteFileAfterCopyingCode.vue
 ┃ ┃ ┃ ┃ ┗ product-slider-OtabekSaveSliderUIComponentHere.vue
 ┃ ┃ ┃ ┣ config
 ┃ ┃ ┃ ┃ ┣ api-constants.js
 ┃ ┃ ┃ ┃ ┗ index.js
 ┃ ┃ ┃ ┣ services
 ┃ ┃ ┃ ┃ ┣ products
 ┃ ┃ ┃ ┃ ┃ ┗ products.js
 ┃ ┃ ┃ ┃ ┗ index.js
 ┃ ┃ ┃ ┗ state
 ┃ ┃ ┃ ┃ ┣ index.js
 ┃ ┃ ┃ ┃ ┗ users-data.js
 ┃ ┃ ┣ wishlist
 ┃ ┃ ┃ ┗ wishlist.vue
 ┃ ┃ ┣ index.js
 ┃ ┃ ┣ products-home.vue
 ┃ ┃ ┣ products-routes.js
 ┃ ┃ ┣ products-state.js
 ┃ ┃ ┗ products.vue
 ┃ ┣ shared
 ┃ ┃ ┣ components
 ┃ ┃ ┃ ┣ app-footer
 ┃ ┃ ┃ ┃ ┗ app-footer.vue
 ┃ ┃ ┃ ┣ app-header
 ┃ ┃ ┃ ┃ ┗ app-header.vue
 ┃ ┃ ┃ ┣ custom-components
 ┃ ┃ ┃ ┃ ┣ components
 ┃ ┃ ┃ ┃ ┃ ┣ choices.js
 ┃ ┃ ┃ ┃ ┃ ┗ primary-search-call.js
 ┃ ┃ ┃ ┃ ┣ forms
 ┃ ┃ ┃ ┃ ┃ ┣ payment-form.vue
 ┃ ┃ ┃ ┃ ┃ ┗ personal-details-form.vue
 ┃ ┃ ┃ ┃ ┣ add-payment-method.vue
 ┃ ┃ ┃ ┃ ┣ delivery-information.vue
 ┃ ┃ ┃ ┃ ┣ hover-dropdown.vue
 ┃ ┃ ┃ ┃ ┣ personal-detail.vue
 ┃ ┃ ┃ ┃ ┣ primary-button.vue
 ┃ ┃ ┃ ┃ ┣ primary-pagination.vue
 ┃ ┃ ┃ ┃ ┣ primary-search.vue
 ┃ ┃ ┃ ┃ ┣ product-card.vue
 ┃ ┃ ┃ ┃ ┣ product-carousel.vue
 ┃ ┃ ┃ ┃ ┣ product-slider-name.vue
 ┃ ┃ ┃ ┃ ┣ product-slider.vue
 ┃ ┃ ┃ ┃ ┣ promotion.vue
 ┃ ┃ ┃ ┃ ┣ rating-stars-message.vue
 ┃ ┃ ┃ ┃ ┣ review-message.vue
 ┃ ┃ ┃ ┃ ┗ slider-arrow.vue
 ┃ ┃ ┃ ┗ error-pages
 ┃ ┃ ┃ ┃ ┗ page-404.vue
 ┃ ┃ ┣ footer
 ┃ ┃ ┃ ┣ footer.vue
 ┃ ┃ ┃ ┣ logo-White.png
 ┃ ┃ ┃ ┣ logo-White.svg
 ┃ ┃ ┃ ┣ mobile-footer.vue
 ┃ ┃ ┃ ┗ web-footer.vue
 ┃ ┃ ┣ global
 ┃ ┃ ┃ ┣ go-back-to-top.vue
 ┃ ┃ ┃ ┣ go-btn.vue
 ┃ ┃ ┃ ┣ go-card.vue
 ┃ ┃ ┃ ┗ go-toggle.vue
 ┃ ┃ ┣ navbar
 ┃ ┃ ┃ ┣ Basket.svg
 ┃ ┃ ┃ ┣ navbar-mobile.vue
 ┃ ┃ ┃ ┣ navbar-web.vue
 ┃ ┃ ┃ ┣ navbar.vue
 ┃ ┃ ┃ ┗ Search.svg
 ┃ ┃ ┣ services
 ┃ ┃ ┃ ┗ index.js
 ┃ ┃ ┗ index.js
 ┃ ┣ users
 ┃ ┃ ┣ shared
 ┃ ┃ ┃ ┣ config
 ┃ ┃ ┃ ┃ ┣ api-constants.js
 ┃ ┃ ┃ ┃ ┗ index.js
 ┃ ┃ ┃ ┣ services
 ┃ ┃ ┃ ┃ ┣ users
 ┃ ┃ ┃ ┃ ┃ ┗ users.js
 ┃ ┃ ┃ ┃ ┗ index.js
 ┃ ┃ ┃ ┗ state
 ┃ ┃ ┃ ┃ ┣ index.js
 ┃ ┃ ┃ ┃ ┗ users-data.js
 ┃ ┃ ┣ user-item
 ┃ ┃ ┃ ┗ user-item.vue
 ┃ ┃ ┣ user-list
 ┃ ┃ ┃ ┗ user-list.vue
 ┃ ┃ ┣ index.js
 ┃ ┃ ┣ users-routes.js
 ┃ ┃ ┣ users-state.js
 ┃ ┃ ┗ users.vue
 ┃ ┗ app-state.js
 ┣ assets
 ┃ ┣ accounts-icons
 ┃ ┃ ┣ discountIcon.svg
 ┃ ┃ ┣ go-avatar.svg
 ┃ ┃ ┣ go-box.svg
 ┃ ┃ ┣ go-credit-card.svg
 ┃ ┃ ┣ go-security.svg
 ┃ ┃ ┣ humo.jpg
 ┃ ┃ ┣ mastercard.svg
 ┃ ┃ ┣ payme.svg
 ┃ ┃ ┣ star.svg
 ┃ ┃ ┗ uzcard.png
 ┃ ┣ bin.svg
 ┃ ┣ click.png
 ┃ ┣ discountIcon.svg
 ┃ ┣ go-logo.svg
 ┃ ┣ left-arrow.svg
 ┃ ┣ mountain.jpg
 ┃ ┣ payme.svg
 ┃ ┣ quasar-logo-full.svg
 ┃ ┣ right-arrow.svg
 ┃ ┗ sad.svg
 ┣ boot
 ┃ ┣ .gitkeep
 ┃ ┣ addressbar-color.js
 ┃ ┣ axios.js
 ┃ ┣ filter.js
 ┃ ┣ i18n.js
 ┃ ┗ register-my-component.js
 ┣ css
 ┃ ┣ fonts
 ┃ ┃ ┣ Franchise
 ┃ ┃ ┃ ┣ Franchise-Bold-hinted.ttf
 ┃ ┃ ┃ ┗ Franchise-Bold.ttf
 ┃ ┃ ┗ Open_Sans
 ┃ ┃ ┃ ┣ LICENSE.txt
 ┃ ┃ ┃ ┣ OpenSans-Bold.ttf
 ┃ ┃ ┃ ┣ OpenSans-BoldItalic.ttf
 ┃ ┃ ┃ ┣ OpenSans-ExtraBold.ttf
 ┃ ┃ ┃ ┣ OpenSans-ExtraBoldItalic.ttf
 ┃ ┃ ┃ ┣ OpenSans-Italic.ttf
 ┃ ┃ ┃ ┣ OpenSans-Light.ttf
 ┃ ┃ ┃ ┣ OpenSans-LightItalic.ttf
 ┃ ┃ ┃ ┣ OpenSans-Regular.ttf
 ┃ ┃ ┃ ┣ OpenSans-SemiBold.ttf
 ┃ ┃ ┃ ┗ OpenSans-SemiBoldItalic.ttf
 ┃ ┣ app.scss
 ┃ ┗ quasar.variables.sass
 ┣ i18n
 ┃ ┣ en-us
 ┃ ┃ ┗ index.js
 ┃ ┣ ru-rus
 ┃ ┃ ┗ index.js
 ┃ ┣ uz-lat
 ┃ ┃ ┗ index.js
 ┃ ┗ index.js
 ┣ layouts
 ┃ ┣ drawer
 ┃ ┃ ┗ drawer.vue
 ┃ ┣ dropdown
 ┃ ┃ ┗ dropdown.vue
 ┃ ┣ checkout-layout.vue
 ┃ ┣ ecom.vue
 ┃ ┣ layout.vue
 ┃ ┗ only-header.vue
 ┣ pages
 ┃ ┗ Error404.vue
 ┣ router
 ┃ ┣ index.js
 ┃ ┗ main-routes.js
 ┣ services
 ┃ ┣ api.service.js
 ┃ ┣ storage.service.js
 ┃ ┗ user.service.js
 ┣ statics
 ┃ ┣ icons
 ┃ ┃ ┣ apple-icon-120x120.png
 ┃ ┃ ┣ apple-icon-152x152.png
 ┃ ┃ ┣ apple-icon-167x167.png
 ┃ ┃ ┣ apple-icon-180x180.png
 ┃ ┃ ┣ Basket.svg
 ┃ ┃ ┣ favicon-16x16.png
 ┃ ┃ ┣ favicon-32x32.png
 ┃ ┃ ┣ favicon-96x96.png
 ┃ ┃ ┣ favicon.ico
 ┃ ┃ ┣ icon-128x128.png
 ┃ ┃ ┣ icon-192x192.png
 ┃ ┃ ┣ icon-256x256.png
 ┃ ┃ ┣ icon-384x384.png
 ┃ ┃ ┣ icon-512x512.png
 ┃ ┃ ┣ ms-icon-144x144.png
 ┃ ┃ ┣ safari-pinned-tab.svg
 ┃ ┃ ┗ Search.svg
 ┃ ┣ products
 ┃ ┃ ┣ bracelet.jpg
 ┃ ┃ ┣ console-controller.jpg
 ┃ ┃ ┣ console-controller2.jpg
 ┃ ┃ ┣ earrings.jpg
 ┃ ┃ ┣ iphone-bridge.jpg
 ┃ ┃ ┣ man-iphone.jpg
 ┃ ┃ ┣ modern-time.jpg
 ┃ ┃ ┣ student-iphone.jpg
 ┃ ┃ ┣ white-faced-watch.jpg
 ┃ ┃ ┗ woman-photo-food.jpg
 ┃ ┣ 404-bg.svg
 ┃ ┣ a.svg
 ┃ ┣ app-logo-128x128.png
 ┃ ┣ comingsoon.png
 ┃ ┣ discountIcon.svg
 ┃ ┣ go-logo.svg
 ┃ ┣ Logo-Black.svg
 ┃ ┣ nintendo-main.png
 ┃ ┣ nintendo.jpg
 ┃ ┣ nintendo1.jpg
 ┃ ┣ nintendo1.png
 ┃ ┣ Nintendo2.jpeg
 ┃ ┣ nintendo3.png
 ┃ ┣ nintendo4.jpg
 ┃ ┣ nintendowhite.png
 ┃ ┣ pattern.svg
 ┃ ┣ product1.png
 ┃ ┣ product2.png
 ┃ ┣ product3.png
 ┃ ┣ product4.png
 ┃ ┗ quasar-logo-full.svg
 ┣ store
 ┃ ┣ module-example
 ┃ ┃ ┣ actions.js
 ┃ ┃ ┣ getters.js
 ┃ ┃ ┣ index.js
 ┃ ┃ ┣ mutations.js
 ┃ ┃ ┗ state.js
 ┃ ┣ auth.js
 ┃ ┣ cart-store.js
 ┃ ┣ index.js
 ┃ ┣ store-auth.js
 ┃ ┣ store-card.js
 ┃ ┣ store-flag.d.ts
 ┃ ┣ store-search-products.js
 ┃ ┗ store-tasks.js
 ┣ a.svg
 ┣ App.vue
 ┗ index.template.html
#### (or sometimes if you don't have yarn write `` npm instal``) and hit enter.
### 3. Project installed succesfully
### 4. If there is/are error/s just see. Something may not be installed. To install 
```css
yarn add/npm intall and package name
```
### 5.If while installation it says choose a icon package manager: pngquant. And after thay write this hashed color code: `` #ff4733 ``

### Then just run this code in terminal again
```css
quasar dev
```

## MAIN FOLDERS OF THE PROJECT

```
Root directory
└─SRC
    └─
└─SRC
    └─
```