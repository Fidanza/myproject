---
title: Cordova
---

# Installing and using Cordova

::: tip
I am writing commands with ``$`` or ``$sudo`` just don't use it in windows. It is for some kind of Linux distros and MacOS. Sometimes all of them may not work in Linux and MacOS. Then if it is written with ``$sudo`` try ``$`` sudo or if it is ``$sudo`` try to write it without ``sudo``. But note for that these are used if any of them doesn't work. And even you can try without anything if all of above doesn't work. Because distros are being devloped day bay day.
::: 

## Installation
- Go root directory with ``CMD/Terminal``
- Then write ``$ yarn global add cordova``. If sometimes you don't have yarn write ``$sudo npm install -g cordova ``) and hit enter key.
- Cordova installed succesfully
- Now to create a new cordova project write ``$ cordova create my-app com.my-company.my-app my-app``.

::: warning
After the project is created, write your app name and package name then again project name. If you don't write the project name and app name there will be errors in the future. Here I am using my-app as an example, you write your app name here. And note for one thing. Android Studio(on Windows or Linux and MacOS) or Swift(on MacOS) must be installed in order to use cordova and create our applications.
:::

## Adding platforms to Cordova

**To create and run platform projects with Cordova run below scripts one by one**
```bash
$ cd my-app
$ cordova platform add browser/android/ios/or others
```

## Mostly used commands for CMD/Terminal

```bash
    cordova create <path> # create new cordova project
    cordova help create #if you don't know how to create
    cd my-app # go to created app
    cordova plugin add <platform name> --save # adding plugin
    cordova platform # for a complete list of platforms
    cordova platform add <platform name> --save # adding platform
    cordova requirements <platform name> # check! is there requirements or not
    cordova build <platform name> --verbose # build. most cases use --verbose
    cordova run <platform name> # run on emulator on the pc
```

## Full explanation for commands.

Above you're seeing ``cd my-app``. This means you're entering the project which is nearly creaed by writing ``cordova create <path-name-here>``. And the my-app is that path name. Then to install additional library(Cordova Plugin Camera) of Cordov you're writing ``cordova plugin add cordova-plugin-camera`` and saving it by adding `` --save`` in the end. You can freely write what the plugin has in Cordova. I shared a link to Cordova Docs in Github in the end. Then you're checking are all the requirements are successfully installed or not in the project with this ``cordova requirements android`` for only Android devices. To check all of the platforms just remove android. Or to check for other platform just replace its name with android.
Then you're building android project with ``cordova build android --verbose``. Then running app in Andoid studio emulator with ``cordova run android`` script. As always, just replace android with the platform you wanted to run, build or checking.


## Advanced Cordova
Below you see some configuration files for ``project/src-cordova/config.xml`` file.
```bash
    <preference name="StatusBarOverlaysWebView" value="true" />
    <preference name="StatusBarBackgroundColor" value="#F44336" />
```

And here the first line is for setting Statusbar overlayed.
Second line is for setting app statusbar color.
Always put this ``preferences`` after ``platform`` closing tag

::: warning
To use it successfully just firstly create debug and release keys.
:::

Use these every lines of code separately and please save every credential to somwhere you can find after 50 years too.
```bash
keytool -genkey -v -keystore quasar-release-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias quasar-release
keytool -importkeystore -srckeystore quasar-release-key.jks -destkeystore quasar-release-key.jks -deststoretype pkcs12
keytool -genkey -v -keystore quasar-debug-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias quasar-debug
keytool -importkeystore -srckeystore quasar-debug-key.jks -destkeystore quasar-debug-key.jks -deststoretype pkcs12
```

 Now we need to create a json configuration file. We'll call that build.cordova.json.

```json
{
    "android": {
        "debug": {
            "keystore": "../quasar-debug-key.jks",
            "storePassword": "*********",
            "alias": "quasar-debug",
            "password" : "*********",
            "keystoreType": "jks"
        },
        "release": {
            "keystore": "../quasar-release-key.jks",
            "storePassword": "*********",
            "alias": "quasar-release",
            "password" : "*********",
            "keystoreType": "jks"
        }
    }
}
```

Once created, we'll need to move the build.cordova.json, quasar-debug-key.jks and quasar-release-key.jks to the root directory of the project. Don't forget to include them in the .gitignore.

And do not forget to change scripts section of package.json file which is in root directory.
```json
{
  "scripts": {
    "lint": "eslint --ext .js,.vue src",
    "test": "echo \"No test specified\" && exit 0",
    "cordova:dev": "quasar dev -m cordova -T android -- --buildConfig=\"../build.cordova.json\"",
    "cordova:build": "quasar build -m cordova -T android -- --buildConfig=\"../build.cordova.json\""
  }
}
```
Now, instead of running quasar dev or quasar build, you would run yarn cordova:dev or yarn cordova:build respectively.
```bash
yarn cordova:dev
```

## Discover Fingerprint of the Keystore.
Our project is already running and signed (even while debugging), but if you're planning to integrate your app with other apps (like Google or Facebook), they will request a fingerprint of your app/certificate.
In order to discover the fingerprint of our app, we would run the following command:
```bash
keytool -v -list -keystore quasar-debug-key.jks
keytool -v -list -keystore quasar-release-key.jks
```

You'll be able to see something like this:
```bash
Debug:   4E:96:80:2D:B8:C6:A9:44:D6:15:7B:FC:54:79:B6:45:C8:26:43:90
Release: 60:64:04:26:71:71:B4:AA:BC:1F:68:EC:2D:0B:59:06:A3:E5:2F:81
```
That's it! A faster way to sign your APK while building.

If you seem this is too hard to do just search go youtube and search:

Signing and aligning Quasar and VueJS application. You'll find it.
**Here find** ***[More info and docs about Cordova](https://github.com/apache/cordova)***