---
title: Moment
---


# Installing and using Moment JS

## Installation

To install and use firstly add it to your project with:
```bash{2,4}
# npm
npm install moment --save
# #Yarn
yarn add moment
```

## Usage

### Format Dates

```js
moment().format('MMMM Do YYYY, h:mm:ss a'); // April 1st 2020, 12:16:54 am
moment().format('dddd');                    // Wednesday
moment().format("MMM Do YY");               // Apr 1st 20
moment().format('YYYY [escaped] YYYY');     // 2020 escaped 2020
moment().format();                          // 2020-04-01T00:16:54+05:00
```


### Relative Time

```js
moment("20111031", "YYYYMMDD").fromNow(); // 8 years ago
moment("20120620", "YYYYMMDD").fromNow(); // 8 years ago
moment().startOf('day').fromNow();        // 17 minutes ago
moment().endOf('day').fromNow();          // in a day
moment().startOf('hour').fromNow();       // 17 minutes ago
```

### Calendar Time

```js
moment().subtract(10, 'days').calendar(); // 03/22/2020
moment().subtract(6, 'days').calendar();  // Last Thursday at 12:16 AM
moment().subtract(3, 'days').calendar();  // Last Sunday at 12:16 AM
moment().subtract(1, 'days').calendar();  // Yesterday at 12:16 AM
moment().calendar();                      // Today at 12:16 AM
moment().add(1, 'days').calendar();       // Tomorrow at 12:16 AM
moment().add(3, 'days').calendar();       // Saturday at 12:16 AM
moment().add(10, 'days').calendar();      // 04/11/2020
```

### Multiple Locale Support

```js
moment.locale();         // en
moment().format('LT');   // 12:16 AM
moment().format('LTS');  // 12:16:54 AM
moment().format('L');    // 04/01/2020
moment().format('l');    // 4/1/2020
moment().format('LL');   // April 1, 2020
moment().format('ll');   // Apr 1, 2020
moment().format('LLL');  // April 1, 2020 12:16 AM
moment().format('lll');  // Apr 1, 2020 12:16 AM
moment().format('LLLL'); // Wednesday, April 1, 2020 12:16 AM
moment().format('llll'); // Wed, Apr 1, 2020 12:16 AM
```

::: warning
As well as, This plugin maybe used to convert data or time coming from backend.
Namely, API or Database.
:::