---
title: Directory structure
---

# Directory structure

## Production folder
- **dist** - production folders set
    - **spa** - production folder for the single page application
    - **pwa** -  production folder for the progressive web application
    - **bex** - production folder for the browser extension application
    - **ssr** - production folder for server side rendering  application
    - **cordova** - production folder for cordova applications for mobile
      - **android** - cordova production folder for android mobile applications
      - **ios** - cordova production folder for ios mobile applications 
    - **capaciror** - production folder for capacitor applications for mobile
      - **android** - capacitor production folder for android mobile applications
      - **ios** - capacitor production folder for ios mobile applications 
    - **electron** -  production folder for the electron applications
      - **windows** - electron production folder for windows applications
      - **mac** - electron production folder for mac applications


## Documentation folder.

- **docs** - documentation folder.
  - **.vupress**(folder) - for the docs configuration
    - **components** - folder for .vue components which are used in the docs
    - **public** - folder for additional files, assets, images and more
    - **styles** - basic/additional style folder. Gets .styl files only.
    - **templates** - additional HTML layouts and templates folder
    - **theme** -  additional theme styling and configuration folder
    - **enhanceApp.js** -  a file for injecting code to make website very fast
    - **config.js** - basic settings file for configuration of the whole docs. Main file of the docs.
  - **Topic**(folders) - like introduction, extensions, plugins and etc which contains markdown files and menus of this documentation.
  - **README.md** - used to style the homepage of the documentation.
  - **Note.txt** - Instructions for running the docs and adding new info and files to it.This was for the person who has never run this docs. But you're seeing this docs I think.
  - **config.md** - description coming soon...

## Source folder

- **src** - main developemnt source folder.
  - **app** - folder for the all components and pages
  - **assets** - folder for svg and some other assets files
  - **boot** - global folder for the boot js files which run before the vue instance initilized
  - **css** - global folder for the all css, scss, sass and stylus files
  - **i18n** - folder for the project language translation files
  - **layouts** - folder for the all layouts of the pages
  - **router** - folder for the main routing system of the project
  - **services** - folder for all js service(request and token) files
  - **statics** - folder for static files such as pictures and icons
  - **store** - folder for the importing and vuex files
  - **app.vue** - a main vuejs file for icluding layouts using router view
  - **index.template.html** - a file which will be converted into index.html after compilation 