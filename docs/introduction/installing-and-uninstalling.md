---
title: Installing and uninstalling
---

# Installing and uninstaling

## Installing packages

To install any package that exists in yarn or npm packages list, you just use the following commands.

```bash{2,5}
# using yarn(recommended)
yarn add [package-name]

# using npm
npm install [package-name]
```
Above code just adds package to the main list. And to add packages into dev-packages(dev-dependencies) you can use below scripts.

```bash{2,4}
# using yarn(recommended)
yarn add [package-name] -D

npm install [package-name] --dev
```

And when sometimes our added packages will not work anymore, so what should you do is saving them. You can see how to save dependencies when not saved automatically in the below scripts.

```bash{2,5,8,11}
# using yarn(recommended)
yarn add [package-name] --save

# using yarn to add dev-dependencies
yarn add [package-name] -D --save

# using npm
npm install [package-name] --save

# using yarn to add dev-dependencies
npm install [package-name] --save-dev
```

## Uninstalling packages

To remove any package that exists in yarn or npm packages list, you just use the following commands.

```bash{2,5}
# using yarn(recommended)
yarn remove [package-name]

# using npm
npm remove [package-name]
```
Above code just removed package from the main list. And to remove dev-packages(dev-dependencies) you can use below scripts.

```bash{2,4}
# using yarn(recommended)
yarn remove [package-name] -D

npm remove [package-name] --dev
```

And when sometimes **remove** doesn't not work with packages or dev-packages anymore, so what should you do is removing saved one. You can see how to remove saved dependencies  in the below scripts.

```bash{2,5,8,11}
# using yarn(recommended)
yarn remove [package-name] --save

# using yarn to remove dev-dependencies
yarn remove [package-name] -D --save

# using npm
npm remove [package-name] --save

# using yarn to remove dev-dependencies
npm remove [package-name] --save-dev
```