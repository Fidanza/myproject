---
title: Get Started
---

# Get started

::: tip
Here we're beginning from cloning the **GoBazar Frontend project(From now, we will briefly refer this as "project")** using **git**.
But if you want to begin from scratch installation of Quasar. You can simply go to the next page.
:::

Ok, let's clone the project. And out project link is: *https://gitlab.com/ikudrat/gofront*. So we use git to install it. If you don't the git app installed on your device, go through the below link and install it using it [Installation of the git on the different devices](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

OK, If now we have git we can open CMD/Terminal and write following script to download the project. And note that you're cloning it into a safe disk and folder.

```bash
git clone https://gitlab.com/ikudrat/gofront
```

Now the project is cloned and we can go to the folder which is mentioned while clone process. In general, it is **gofront**, so we can use the below script to enter the project directory.

```bash
cd gofront
```

::: warning
If you want to work on another branch, not on the master branch, you can easily switch to that branch which you want, with git checkout and branch name. For instance, if you want to go to the dev branch, write **git checkout dev**.
:::

And if we're on the correct branch we need to install Vue(Quasar, Modules Extensions, Plugins and more). For this, we need a package manager. So the two famous package managers: **NPM** and **YARN** can be used with the project. To install with npm you say: ``npm install`` or ``yarn``. When installing with yarn you can say: ``yarn install`` too. This works fine when the yarn script is not supported.

```bash{2,4}
# With NPM // Not Recommended
npm install
# With yarn // Recommended
yarn
# or yarn install when yarn not worked
```
OK, now our project is successfully working on the **dev server**.
For more about quasar scripts like building and more go to the page of **Quasar commands**.