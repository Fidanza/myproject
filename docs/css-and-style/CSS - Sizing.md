---
title: Sizing
---

BEFORE READING: GET ACQUAINTED WITH README.MD AND ABOVE 1-2-FOLDERS TOO.

There is no much thing here to read. Just not that there is a ``sizekit.css`` file in ``src/css/`` directory. And you can see sizing classes there. Just use them freely how much you want in any HTML and .VUE files. You can use them inside of the css but we didn't add that opportunity. **THAT IS BECAUSE RATHER THAN WRITING ``var(--size50)`` WRITING ``50px`` IS MUCH MORE EASIER**.