import Vue from 'vue'
import {i18n} from 'src/boot/i18n'
import da from "quasar/lang/da";

Vue.filter('truncate', function (text, length, suffix) {
  if (text.length > length) {
      return text.substring(0, length) + suffix;
  } else {
      return text;
  }
});


Vue.filter('sum', function (data) {
  if (data != undefined){
    data = (data == String) ? data : data.toString()
    data = (data.search(/\./) != -1) ? data.substring(0, data.indexOf('.')) : data
    data = Number(data)
    data = data.toLocaleString('ru-RU');
    if (localStorage.getItem('lang')) i18n.locale = localStorage.getItem('lang');
    return data + ' ' + i18n.t('sum')
  }
});

Vue.filter('calc_price', function (price, quantity) {
  if (quantity != undefined){
    var calculated_price = 0
    calculated_price = price * quantity
    calculated_price = Number(calculated_price)
    calculated_price = calculated_price.toLocaleString('ru-RU');
    if (localStorage.getItem('lang')) i18n.locale = localStorage.getItem('lang');
    return calculated_price + ' ' + i18n.t('sum')
  }
});

Vue.filter('formatNum', function (data) {
  if (data != undefined){
    data = Number(data)
    data = data.toLocaleString('ru-RU');
    return data
  }
});

