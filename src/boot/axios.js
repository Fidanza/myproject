import Vue from 'vue'
import axios from 'axios'
import ApiService from '../services/api.service'
import { TokenService } from '../services/storage.service'

Vue.prototype.$axios = axios

// let BASE_URL = "http://localhost:8000"
let BASE_URL = "https://api.gobazar.com"


ApiService.init(BASE_URL)

if (TokenService.getToken()) {
  ApiService.setHeader()
  ApiService.mount401Interceptor()
} else {
  ApiService.setLanguageToHeader()
}

Vue.prototype.$baseURL = 'http://api.gobazar.com:8000'
  // Vue.prototype.$mediaURL = "https://gobazar.s3.amazonaws.com"


// const axiosInstance = axios.create({
//   baseURL: 'https://api.example.com'
// })

// export default ({ Vue }) => {
//   Vue.prototype.$axios = axios
// }
// export { axiosInstance }

