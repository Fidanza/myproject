import ApiService from './api.service'


class AuthenticationError extends Error {
    constructor(errorCode, message) {
        super(message)
        this.name = this.constructor.name
        this.message = message
        this.errorCode = errorCode
    }
}
const addressService = {
    /**
     * Login the user and store the access token to TokenService. 
     * 
     * @returns access_token
     * @throws AuthenticationError 
    **/


    getListAddress: async function(){
        const requestData = {
            method: 'get',
            url: "/api/addresses/v1/addresses",
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    }, 

    createAddress: async function(data){
        const requestData = {
            method: 'post',
            url: "/api/addresses/v1/create_address",
            data: data
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },

    setDefault: async function(address_id){
        const requestData = {
            method: 'put',
            url: "/api/addresses/v1/set_default_address/"+address_id,
            data: {is_default:true}
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    getEditAddress: async function(address_id){
        const requestData = {
            method: 'get',
            url: "/api/addresses/v1/update_address/"+address_id
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    editAddress: async function(address_id, data){
        const requestData = {
            method: 'put',
            headers: {
                'Content-Type': 'application/json',
            },
            url: "/api/addresses/v1/update_address/"+address_id,
            data: data
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.status
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    deleteAddress: async function(address_id){
        const requestData = {
            method: 'put',
            url: "/api/addresses/v1/delete_address/"+address_id,
            data: {
                "is_deleted": true
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    userCreatePaymentCard: async function(payload){
        const requestData = {
            method: 'post',
            url: "/api/billing/v1/token",
            data: {
                number: payload.cardNum,
                expire: payload.expire
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    sendSMSCodeVerfy: async function(payload){
        const requestData = {
            method: 'post',
            url: "/api/billing/v1/verify/create",
            data: {
                sms: payload.sms,
                token: payload.token,
                phone: payload.phone,
                name: payload.name
            }
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    getHomePageProducts: async function(){
        const requestData = {
            method: 'get',
            url: "/api/products/v1/homeproducts",
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    newHomePageProducts: async function(){
        const requestData = {
            method: 'get',
            url: "/api/products/v1/homeproducts/list",
        }

        try {
            const response = await ApiService.customRequest(requestData)
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },
    sendSupport: async function(data){
        const requestData = {
            method: 'post',
            url: "api/customer/v1/support/",
            data: data
        }
        try {
            const response = await ApiService.customRequest(requestData)
            return response
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },


    
}

export default addressService

export { addressService, AuthenticationError }