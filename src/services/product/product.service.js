import ApiService from "../api.service";
const ProductService = {
  async productDetails(productID){
 

      const requestData = {
        method: 'get',
        url: `/api/products/v1/product/detail/?itemId=${productID}`
      }
      try {
        let response = await ApiService.customRequest(requestData)
        return response
      } catch (e) {
       console.log('error in details')
      }
    
  
    
  },

  productReviews: async function(productID){
    const requestData = {
      method: 'get',
      url: `/api/reviews/v1/product_review/${productID}`
    }
    try {
      const response = await ApiService.customRequest(requestData)
      return response
    } catch (e) {
     console.log(e)
    }
  },

  productRatings: async function(productID){
    const requestData = {
      method: 'get',
      url: `/api/reviews/v1/rating_statistic/${productID}`
    }
    try {
      const response = await ApiService.customRequest(requestData)
      return response
    } catch (e) {

    }
  }
}

export default ProductService
