import ApiService from "../api.service";

const ProductList = {
  async searchProducts(payload){
    let url = payload
    let params = new URLSearchParams(url.search.slice(1));
    let aspectValues = ""
    let i = 0
    while (params.has('name'+i)){
      let name = params.get('name'+i)
      let values = params.get('value'+i)
      values = values.split("|")
      values.forEach(function (value){
        aspectValues += "&name="+name+"&value="+value
      })
      i++;
    }
    
    const requestData = {
      method: 'get',
      url: `api/products/v1/multiple/filters?${aspectValues}`,
      params: {
        keyword: params.get("k"),
        catid: params.get("cid") && params.get("cid"),
        min: params.get("min") && params.get("min"),
        max: params.get("min") && params.get("max"),
        sorting: params.get("st") && params.get("st"),
        pagenum: params.get("cpn") && params.get("cpn"),
        productsPerPage: 20
     }
    }
    try {
      const response = await ApiService.customRequest(requestData)
      return response
    } catch (error) {
      console.log('SEARCH PRODUCT HAS ERROR', error)
    }
  },
  async aspectList(keyword) {
    const requestData = {
      method: 'get',
      url: `api/products/v1/search/aspect/?keyword=${keyword}`
    }
    try {
      let response = await ApiService.customRequest(requestData)
      return response
    } catch (e) {
      console.log('Error in Aspect List service')
    }
  },
  async fetchProductList(searchKeyword, productPerPage, pageNum) {
    const requestData = {
      method: 'get',
      url: `api/products/v1/search/keyword/?keyword=${searchKeyword}&productsPerPage=${productPerPage}&pagenum=${pageNum}`
    }
    try {
      let response = await ApiService.customRequest(requestData)
      return response
    } catch (e) {
      console.log('Error in product List service')
    }
  },

  async fetchRootCategoryList(searchKeyword, productPerPage, pageNum) {
    const requestData = {
      method: 'get',
      url: `api/products/v1/search/category/?keyword=${searchKeyword}&productsPerPage=${productPerPage}&pagenum=${pageNum}`
    }
    try {
      let response = await ApiService.customRequest(requestData);
      return response
    } catch (e) {

    }
  },

  async fetchChildCategory(searchKeyword, productsPerPage, pageNum, catID) {
    const requestData = {
      method: 'get',
      url: `api/products/v1/search/category/id/?keyword=${searchKeyword}&productsPerPage=${productsPerPage}&pagenum=${pageNum}&catid=${catID}`
    }
    try {
      let response = await ApiService.customRequest(requestData)
      return response
    } catch (e) {
      console.log('Error in fetching childCategories')
    }
  },

  async fetchAspectList(searchKeyword, productPerPage, pageNum) {
    const requestData = {
      method: 'get',
      url: `api/products/v1/search/aspect/?keyword=${searchKeyword}&productsPerPage=${productPerPage}&pagenum=${pageNum}`
    }

    try {
      let response = await ApiService.customRequest(requestData)

      return response
    } catch (e) {
      console.log('Error in fetching aspectList')
    }
  },

  async fetchProductListByAspects(searchKeyword, productsPerPage, pageNum, aspectValuesList) {
    let aspectValues = ""

    for (const element in aspectValuesList) {
      aspectValues += `&AspectFilter.name=${aspectValuesList[element].aspectName}&AspectFilter.value=${aspectValuesList[element].aspectValue}`
    }

    const requestData = {
      method: 'get',
      url: `api/products/v1/search/aspect/filter/?keyword=${searchKeyword}&productsPerPage=${productsPerPage}&pagenum=${pageNum}${aspectValues}`
    }

    try {
      let response = await ApiService.customRequest(requestData)

      return response
    } catch (e) {

    }
  },

  async fetchSortedProductList(searchKeyword, productsPerPage, pageNum, sortType, minPrice, maxPrice) {
    const requestData = {
      method: 'get',
      url: `api/products/v1/search/${sortType}/?keyword=${searchKeyword}&productsPerPage=${productsPerPage}&pagenum=${pageNum}&min=${minPrice}&max=${maxPrice}`
    }
    try {
      let response = await ApiService.customRequest(requestData)
      return response
    } catch (e) {
      console.log('Error in Sort Request')
    }
  },
  recursivelyUpdateChildrenProperties(node, func, param) {
    if (typeof func !== "function" || !node.hasOwnProperty("children") || node.children.length < 1)
      return;
    const newNodeChildren = node.children.map(child => {
      const newChild = func(node, child, param);
      return {
        ...child,
        ...newChild,
        children: recurseChildren(newChild, newChild.children, func, param)
      };
    });
    function recurseChildren(parent, children, func, param) {
      if (!children)
        return;
      const newChildren = children.map(child => {
        const newChild = func(parent, child, param);
        if (child.hasOwnProperty("children") && child.children.length > 0) {
          return {
            ...child,
            ...newChild,
            children: recurseChildren(newChild, newChild.children, func, param)
          };
        }
        return {
          ...child,
          ...newChild
        };
      });
      return newChildren;
    }
    return newNodeChildren;
  },

  assignChild(tree, levels, n) {
    if (n == 0) {
      return 1
    }
    return tree[levels[n]].assignChild(tree[levels[n - 1]].childCategoryHistogram, n - 1)
  },
}

export default ProductList;
