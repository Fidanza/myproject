// Это всего лишь пример,
// Таким образом, вы можете безопасно удалить все детали по умолчанию ниже
export default {
  // Панель навигации
  searchWord: "Категории",

  userGreeting: 'Здравствуйте',
  register: 'Регистрация',
  createYourAccount: 'Создайте свой аккаунт',
  login: 'Войти',
  loginToYourAccount: 'Войдите в свой аккаунт',
  notSignedUpYet: 'Еще не зарегистрировались?',
  forgotPassword: 'Забыли пароль?',
  alreadySignedUp: 'Уже зарегистрировались?',

  yourAccount: 'Ваша учетная запись',
  myAccount : 'Мой аккаунт',
  yourInfo: 'Ваша информация',
  yourPassword: 'Ваш пароль',
  oldPassword: 'Старый пароль',
  newPassword: 'Новый пароль',
  updateInfo: 'Обновить информацию',
  updatePassword : 'Обновить пароль',
  retryNewPassword: 'Повторите новый пароль',
  yourOrders: 'Ваши заказы',
  addressBook: 'Адресная книга',
  paymentMethods: 'Способы оплаты',
  loginAndSecurity: 'Логин и безопасность',
  wishlist: 'Список пожеланий',
  signOut: 'Выход',
  trending_categories:'Трендовые категории',
  all: 'Все',
  allCategories: "Все категории",
  trending: "Популярные",
  todaysDeals: "Сегодняшние соглашения",
  bestSellers: "Бестселлеры",

  // Главная карусель
  shopNow: 'Купи сейчас',
  withFreeDelivery: 'с бесплатной доставкой',
  from: 'От',
  leave_review:'Оставить отзыв',
  // Панель нижнего колонтитула
  backToTop: 'Вернуться к началу',
  no_cancelled_order:'У вас еще нет отмененных заказов',
  no_order:'У вас еще нет заказов',
  make_default:'Сделать метод по умолчанию',
  // Cart Sticky
  yourBasket: 'Ваша корзина',
  items: 'продукта',
  viewBasket: 'Посмотреть корзину',
  goToCheckout: 'Оформить заказ',

  // Нижний колонтитул
   helpCenter: 'Справочный центр',

  // Your orders
  removeItem: 'Удалить',
  goCheckout: 'Купить',

  // Cart
  delivery: 'Доставка',
  deliveryInfo: 'Эта сумма может варьироваться в зависимости от адреса доставки',
  subtotal: 'Общая',
  total: 'Итог',
  summary: 'Резюме',
  addCoupon: 'Добавить купон',
  minimumValidation: 'Должно быть как минимум',
  quantity: 'Количество',
  moveToWishList: 'Добавить в список желаемого',
  all_orders:'Все заказы',
  open_orders:'Открытый заказы',
  cancelled_orders:'Отмененные заказы',
  order_placed:"Заказ размещен",
  delivered_to:'Доставлено в',
  order_delivered:'Заказ доставлен',
  view_order_detail:"Просмотр сведений о заказе",
  // Address
  addNewAddress: 'Добавить новый адрес доставки',
  setDefaultAddress: 'Сделать По Умолчанию',
  defaultAddress: 'Адрес по умолчанию',
  editAddress: 'Изменить Адрес',
  first_name: 'Имя',
  last_name: 'Фамилия',
  email_address: 'Адрес электронной почты',
  password: 'Введите пароль',
  confirmPassword: 'Еще раз введите пароль',
  address: 'Адрес',
  passport: 'Серия и номер паспорта',
  country: 'Страна',
  region: 'Область',
  city: 'Город',
  postalCode: 'Почтовый индекс',
  updateAddress: 'Обновить адрес',
  deleteAddress: 'Удалить адрес',
  andijan: 'Андижанская обл.',
  bukhara: 'Бухарская обл.',
  fergana: 'Ферганская обл.',
  jizzakh: 'Джизакская обл.',
  xorazm: 'Хорезмская обл.',
  namangan: 'Наманганская обл.',
  navoiy: 'Навоийская обл.',
  qashqadaryo: 'Кашкадарьинская обл.',
  samarqand: 'Самаркандская обл.',
  surxondaryo: 'Сурхандарьинская обл.',
  tashkent: 'Ташкентская обл.',
  karakalpakstan: 'Республика Каракалпакстан',
  tashkentcity: 'Город Ташкент',
  almata: 'Алма-Ата',
  astana: 'Нур-Султан (Астана)',
  uz: 'Узбекистан',
  kz: 'Казахстан',
  are_you_sure: 'Вы уверены?',
  yes_delete_it: 'Да, удали это!',
  cancel: 'Отмена',
  addAddress: 'Добавить адрес',
  contact_seller:"Связаться с продавцом",
  cancel_order:"Отменить заказ",
  brand:'Марка',
  //Checkout
  agree_message: 'Вы должны сначала принять условия',
  terms_and_conditions: 'Условия и положения',
  terms_info: "Добро пожаловать в GoBazar LLS "+

  "Настоящие Условия предоставления услуг («Условия», «Условия предоставления услуг») регулируют использование вами нашего веб-сайта, расположенного по адресу gobazar.com (совместно или отдельно «Сервис»), которым управляет GoBazar LLS. "+

  "Наша Политика конфиденциальности также регулирует использование вами наших Сервисов и объясняет, как мы собираем, защищаем и раскрываем информацию, полученную в результате использования вами наших веб-страниц. "+

  "Ваше соглашение с нами включает в себя настоящие Условия и нашу Политику конфиденциальности («Соглашения»). Вы подтверждаете, что прочитали и поняли Соглашения, и соглашаетесь с ними соблюдать. "+

  "Если вы не согласны (или не можете соблюдать) Соглашения, вы не можете использовать Службу, но, пожалуйста, сообщите нам об этом по электронной почте gobazar@gmail.com, чтобы мы могли попытаться найти решение. Настоящие Условия распространяются на всех посетителей, пользователей и других лиц, которые хотят получить доступ или использовать Сервис.",
  i_agree: 'Я согласен с правилами и условиями',
  sum: 'сум',

  //Payment Methods
  credit_debit_cards: 'Кредитные/Дебетовые Карты',
  add_new_payment: 'Добавить новый способ оплаты',
  card_holder_name: 'Имя владельца карты',
  required: 'Обязательное поле *',
  card_number: 'Номер карты: ',
  expires: 'Истекает: ',
  add_payment_method: 'Добавить способ оплаты',
  something_error: 'Что-то пошло не так, пожалуйста, проверьте это и попробуйте снова!',
  enter_password: 'Введите пароль для подтверждения здесь:',
  resend_sms: 'Повторно выслать смс',
  sent_sms: 'SMS отправлено на номер ниже: ',
  sent_sms_desc: 'Пожалуйста, подождите! SMS было отправлено. Если вы не получили SMS, нажмите кнопку «Повторно выслать смс»',
  waiting_time: 'Время ожидания: ',
  default_method: 'Метод по умолчанию',
  delete: 'Удалять',

  searchInput:"Поиск...",

  personal_info:"Личная информация",
  delivery_address:"Адресс доставки",
  shipping_details:"Отгрузочные реквизиты",
  payment:'Оплата',
  state_provinence:'Страна',
  zip_code:'Почтовый индекс',
  phone_number:"Номер телефона",
  continue:'Продолжить',
  continue_shopping:'Продолжить покупки',
  personal_detail:'Ваша личная деталь',
  phone: 'Телефон',
  notAvailable:'Нет в наличии',
  minOrder: 'Извините, существует ограничение в 5 для каждого типа продукта.',
  addedToCart: 'Товар успешно добавлен в корзину',
  addedToWishlist: 'Товар успешно добавлен в список желаний',
  wishlistinfo:'Продукт уже существует',
  productRemoved: 'Продукт успешно удалён',
  quantityReduced: 'Количество продукта успешно уменьшено',
  quantityIncreased: 'Количество продукта успешно увеличилось',
  increased_by_one: 'Выбранный вами товар увеличился на единицу.',
  estimated_date:'Ваша предполагаемая дата поставки',
  fragileInfo: 'Для хрупкой доставки будет использоваться специальная упаковка. Если товар сломан компанией доставки, то стоимость будет возмещена',
  add_new_address:'Добавить новый адрес',
  add_new_del_address:'Добавить новый адрес доставки',
  select_address:'Выберите адрес',
  back:'Назад',
  standard_delivery:"Стандартная поставка",
  fragile_delivery:"Хрупкая доставка",
  order_details:"Деталь заказа",
  order_reference:'Ссылка на заказ',
  shipping_method:'Способ доставки',
  your_order_confirmed:'Ваш заказ был подтвержден',
  thank_shopping:'Спасибо за покупки с нами',
  view_my_order:"Посмотреть мой заказ",
  order_sent:"Ваш заказ будет отправлен по адресу",
  order_detail_listed:'Детали вашего заказа перечислены ниже',
  confirmation_letter:'Мы отправим вам подтверждение, как только ваш товар будет отправлен. Если вы хотите просмотреть детали вашего заказа позже, вы можете получить доступ к этому заказу в любое время через свою учетную запись и нажав Мои заказы.',

  selectAttribute: 'Вы не выбрали никаких атрибутов. Пожалуйста, выберите любой атрибут.',
  default: 'Лучший выбор',
  sortby: 'Сортировать по',
  filterby: 'Фильтровать по',
  ascending: 'Самые дешевые',
  descending: 'Самые дорогие',
  outOfStock: 'Нет на складе',
  matchPasswords: 'Пожалуйста, убедитесь, что ваши пароли совпадают',
  enterEmail: 'Пожалуйста, введите действительный адрес электронной почты.',
  registerSuccessfully:'Регистрация прошла успешно',
  registerMessage: 'Спасибо. Мы отправили вам по электронной почте ссылку для активации. Пожалуйста, нажмите на ссылку, чтобы активировать свой аккаунт',
  more: 'Больше',
  fewer: 'Меньше',
  showMore: 'Показать больше',
  showLess: 'Показывай меньше',

  buy_again:'Снова купить',
  notFoundMessage: 'Ссылка, на которую вы нажали, может быть сломана или страница может быть удалена',
  smthWentWrongMessage: 'Пожалуйста вернитесь и попробуйте снова или перейдите на домашнюю страницу',
  productNotFound: 'К сожалению, нам не удалось найти продукт, который вы искали. Убедитесь, что название продукта написано правильно или ищите другой продукт',
  commingMessage: 'Мы прилагаем все усилия, чтобы добавить новые функции и улучшить удобство использования нашего сайта. Быть в курсе!',
  supportTitle: 'Мы хотели бы услышать от вас',
  supportMessage: 'Если у вас есть вопрос об особенностях, пробных версиях, ценах, нужна демоверсия <br v-if="windowWidth > 750" /> или что-то еще, наша команда готова ответить на все ваши вопросы',
  contactUs: 'Связаться с нами',
  supportSubject: 'Название вашей темы поддержки',
  subjectMessage: 'Описание вашей темы поддержки',
  supportMessageSended: 'Ваше сообщение поддержки отправлено по электронной почте одному из администраторов Gobazar',
  sendSupport: 'Отправить',
  noProduct: 'Продукт не найден',
  goHome: 'Перейти на главную страницу',
  smthWrong: 'Что-то пошло не так',
  sizeGuide: 'Таблица размеров',
  men:'Мужчины',
  women:'Женшины',
  children: 'Дети',
  babysFootwear: 'Обувь для малышей',
  childrensFootwear: 'Обувь для детей',
  clothingSize: 'Одежда',
  footwearSize: 'Oбувь',
  faqs: 'Часто задаваемые вопросы'
}
