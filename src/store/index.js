import Vue from 'vue'
import Vuex from 'vuex'
// import VueAwesomeSwiper from 'vue-awesome-swiper'
// import 'swiper/dist/css/swiper.css'
import axios from 'axios'

// import example from './module-example'

// import auth2 from './store-auth'

import auth from './auth'
import address from './address'
import profile from './profile'
import checkout from './checkout'

import searchProducts from './store-search-products'
import category from './store-category'


// Vue.use(VueAwesomeSwiper)
Vue.use(Vuex)

Vue.use({
        install(Vue) {
            Vue.prototype.$api = axios.create({
                baseURL: 'http://localhost:8000/api/v1/'
                    // baseURL: 'http://api.gobazar.com/api/v1/'
            })
        }
    })
    /*
     * If not building with SSR mode, you can
     * directly export the Store instantiation
     */

let store = null

export default function() {
    store = new Vuex.Store({
        modules: {
            // example
            searchProducts,
            category,
            address,
            profile,
            checkout,
            auth
        },

        // enable strict mode (adds overhead!)
        // for dev mode only
        strict: process.env.DEV
    })

    return store
}

export { store }
