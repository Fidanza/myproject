import {fabTheRedYeti} from "@quasar/extras/fontawesome-v5";
import ProductList from "../services/product/product-list.service";
import Vue from 'vue'
 
const state = {
  newProductList:'',
  newCategoryList: '',
  keyword:'',
  productListLoader: true,
  searchKeyword: '',
  productListInStore:[],
  loader: true,
  smthWemtWrongMessage: false,
  noProductMessage: false,
  categoryLoader: true,
  totalProducts: "",
  sort: null,
  minPrice: null,
  maxPrice: null,

  // category
  categoryList:[],
  aspectListInStore:[],
  aspectValuesList:[],
  productImageUrl: "",
  totalPages: "",

  //category
  showChildCat: false,
  categoryHistogram:{},
  categoryTrees:[],
  categoryObj:{
   "parent":{
    "level": undefined,
     "categoryId":undefined,
     "categoryName": undefined
   },
   "current":{
    "level": undefined,
    "categoryId":undefined,
    "categoryName": undefined

   },
   'child': []
 },
 categoryID: undefined,
 categoryName: undefined,
 level: undefined,
 showRootCategory: false,
  indexes:[],
  childCounter:0

}

const mutations = {
  newProductList(state, payload){
    state.newProductList = payload.data.products
    if (payload.data.categoryHistogram) {state.newCategoryList = payload.data.categoryHistogram.categoryHistogram; state.showRootCategory = true}
    state.totalPages = (Number(payload.data.pagination.totalPages) > 100) ? 100 : payload.data.pagination.totalPages
    state.totalProducts = payload.data.pagination.totalEntries
    state.loader = false
    state.productListLoader = false
  },

  waiting(state){
      state.productListLoader = true
      state.loader = true
      state.noProductMessage = false
      state.smthWemtWrongMessage = false
  },
  setProductListLoader(state, payload){
    state.productListLoader = payload
  },
  aspects(state,payload){
    function sortAspect( a, b ) {
      if (a._name < b._name){
        return -1;
      }
      if (a._name > b._name){
        return 1;
      }
      return 0;
    }
    if(payload){
      var sortedAspectList = payload.sort(sortAspect);
      var newList = []
      sortedAspectList.forEach((item)=>{
        if(item.valueHistogram.length && item.valueHistogram.length > 0){
          newList.push(item)
        }else{
          var newValue = []
          newValue.push(item.valueHistogram)
          item.valueHistogram = newValue
          newList.push(item)
        }
        
      });
      state.aspectListInStore = newList;
    }
    
  },
  keywordUpdate(state, keyword){
    state.keyword = keyword
  },
  selectedAspects(state){
    state.aspectValuesList = []
  },

  noProductMessage(state){
    state.newProductList = []
    state.noProductMessage = true
    state.loader = false
    state.totalProducts = 0
    state.productListLoader = false

  },
  smthWemtWrongMessage(state){
    state.smthWemtWrongMessage = true
    state.loader = false
  },
  updateCatLoader(state){
    state.categoryLoader = true
  },
 updateLoader(state){
   state.loader = true
 },
 updateSearchKeyword(state, payload){
   state.searchKeyword = payload
 },

 updateExceptionMEssages(state){
   state.smthWemtWrongMessage = false
   state.noProductMessage = false

 },

  setProductLisToState(state, response){
   if(response === undefined){
    state.smthWemtWrongMessage = true
   } else if(response.data.products.length === 0){
    state.noProductMessage = true
   } else {
    state.productListInStore = response.data.products
    state.totalPages = response.data.pagination.totalPages
    state.totalProducts = response.data['pagination']['totalEntries']
    state.loader = false
    state.smthWemtWrongMessage = false
   }

  },

  setRootCategoryListToState(state, payload){

   if(payload.response.data["total_products"]["count"] == 0){
    state.noProductMessage = true
   } else {
    state.noProductMessage = false
    let trees = payload.response.data.categoryHistogram.categoryHistogram
    for(let i =0; i < trees.length; i++){
     let catLevel = i;
     catLevel = catLevel.toString()
     Vue.set(trees[i], 'level', catLevel)
    }

    state.categoryTrees = trees
    if(payload['updateProductList']) {
     state.productListInStore = payload.response.data.products
     state.totalProducts = payload.response.data['pagination']['totalEntries']
     state.loader = false
    }
    state.categoryLoader = false
    state.showRootCategory = true

   }


  },

  setShowCategoryToState(state, payload){
    for(let i = 0; i < state.categoryTrees.length; i++){
      if(payload === i){
       Vue.set(state.categoryTrees[i], 'show', true)
      } else {
       Vue.set(state.categoryTrees[i], 'show', false)
      }
    }
  },

 setChildCategoryToState(state, catObj){
  if(catObj.response === undefined){
   state.smthWemtWrongMessage = true
  } else if(catObj.response.data.products.length === 0){
   state.noProductMessage = true
  } else {
   let parentTrees, childTrees, catLevel, levels
   state.showRootCategory = false
   state.categoryID = catObj.catID
   state.categoryName = catObj.categoryName
   state.level = catObj.level


   parentTrees = state.categoryTrees
   if (catObj.response.data['categoryHistogram']) {

    childTrees = catObj.response.data.categoryHistogram.categoryHistogram[0]['childCategoryHistogram']
    for (let i = 0; i < childTrees.length; i++) {
     catLevel = catObj.level + '_' + i
     catLevel = catLevel.toString()
     Vue.set(childTrees[i], 'level', catLevel)

    }
   }
   levels = catObj.level.split('_')
   if (parentTrees.length === 0) {
    this.$router.push({name: 'search', query: {k: catObj.skeyword, cpn: 1}})
   } else {
    if (levels.length === 1) {
     state.categoryObj.parent.level = undefined
     state.categoryObj.parent.categoryId = undefined
     state.categoryObj.parent.categoryName = undefined

     state.categoryObj.current.level = catObj.level
     state.categoryObj.current.categoryId = catObj.catID
     state.categoryObj.current.categoryName = catObj.categoryName

     if (catObj.response.data.categoryHistogram) {
      state.categoryObj.child = childTrees
      if (parentTrees[levels[0]].child) {
       parentTrees[levels[0]].child = childTrees
      } else {
       Vue.set(parentTrees[levels[0]], 'child', childTrees)
      }

     } else {
      state.categoryObj.child = []
     }

    } else if (levels.length === 2) {

     state.categoryObj.parent.level = parentTrees[levels[0]].level
     state.categoryObj.parent.categoryId = parentTrees[levels[0]].categoryId
     state.categoryObj.parent.categoryName = parentTrees[levels[0]].categoryName

     state.categoryObj.current.level = catObj.level
     state.categoryObj.current.categoryId = catObj.catID
     state.categoryObj.current.categoryName = catObj.categoryName
     // state.categoryObj.child = childTrees

     if (catObj.response.data.categoryHistogram) {
      state.categoryObj.child = childTrees
      Vue.set(parentTrees[levels[0]].child[levels[1]], 'child', childTrees)
     } else {
      state.categoryObj.child = []
     }

    } else if (levels.length === 3) {

     state.categoryObj.parent.level = parentTrees[levels[0]].child[levels[1]].level
     state.categoryObj.parent.categoryId = parentTrees[levels[0]].child[levels[1]].categoryId
     state.categoryObj.parent.categoryName = parentTrees[levels[0]].child[levels[1]].categoryName

     state.categoryObj.current.level = catObj.level
     state.categoryObj.current.categoryId = catObj.catID
     state.categoryObj.current.categoryName = catObj.categoryName
     // state.categoryObj.child = childTrees

     if (catObj.response.data.categoryHistogram) {
      state.categoryObj.child = childTrees
      Vue.set(parentTrees[levels[0]].child[levels[1]].child[levels[2]], 'child', childTrees)
     } else {
      state.categoryObj.child = []
     }

    } else if (levels.length === 4) {

     state.categoryObj.parent.level = parentTrees[levels[0]].child[levels[1]].child[levels[2]].level
     state.categoryObj.parent.categoryId = parentTrees[levels[0]].child[levels[1]].child[levels[2]].categoryId
     state.categoryObj.parent.categoryName = parentTrees[levels[0]].child[levels[1]].child[levels[2]].categoryName

     state.categoryObj.current.level = catObj.level
     state.categoryObj.current.categoryId = catObj.catID
     state.categoryObj.current.categoryName = catObj.categoryName
     // state.categoryObj.child = childTrees

     if (catObj.response.data.categoryHistogram) {
      state.categoryObj.child = childTrees
      Vue.set(parentTrees[levels[0]].child[levels[1]].child[levels[2]].child[3], 'child', childTrees)
     } else {
      state.categoryObj.child = []
     }

    } else if (levels.length === 5) {


     state.categoryObj.parent.level = parentTrees[levels[0]].child[levels[1]].child[levels[2]].child[levels[3]].level
     state.categoryObj.parent.categoryId = parentTrees[levels[0]].child[levels[1]].child[levels[2]].child[levels[3]].categoryId
     state.categoryObj.parent.categoryName = parentTrees[levels[0]].child[levels[1]].child[levels[2]].child[levels[3]].categoryName

     state.categoryObj.current.level = catObj.level
     state.categoryObj.current.categoryId = catObj.catID
     state.categoryObj.current.categoryName = catObj.categoryName
     // state.categoryObj.child = childTrees

     if (catObj.response.data.categoryHistogram) {
      state.categoryObj.child = childTrees
      Vue.set(parentTrees[levels[0]].child[levels[1]].child[levels[2]].child[3].child[4], 'child', childTrees)
     } else {
      state.categoryObj.child = []
     }

    } else if (levels.length === 6) {

     state.categoryObj.parent.level = parentTrees[levels[0]].child[levels[1]].child[levels[2]].child[levels[3]].child[levels[5]].level
     state.categoryObj.parent.categoryId = parentTrees[levels[0]].child[levels[1]].child[levels[2]].child[levels[3]].child[levels[5]].categoryId
     state.categoryObj.parent.categoryName = parentTrees[levels[0]].child[levels[1]].child[levels[2]].child[levels[3]].child[levels[5]].categoryName

     state.categoryObj.current.level = catObj.level
     state.categoryObj.current.categoryId = catObj.catID
     state.categoryObj.current.categoryName = catObj.categoryName
     // state.categoryObj.child = childTrees

     if (catObj.response.data.categoryHistogram) {
      state.categoryObj.child = childTrees
      Vue.set(parentTrees[levels[0]].child[levels[1]].child[levels[2]].child[3].child[4].child[5], 'child', childTrees)
     } else {
      state.categoryObj.child = []
     }
    }

    state.productListInStore = catObj.response.data.products
    state.totalProducts = catObj.response.data['pagination']['totalEntries']
    state.categoryTrees = parentTrees
    state.categoryLoader = false
    state.loader = false
    state.totalPages = catObj.response.data.pagination['totalPages']
   }
   state.loader = false
   state.smthWemtWrongMessage = false
  }
  },
  setAspectListToState(state, response){

    state.aspectListInStore = response.data['aspectHistogram']['aspect']
  },

  setProductListByAspectToState(state, response){

   if(response == undefined){
    state.smthWemtWrongMessage = true
   } else if(response.data.products.length == 0){
    state.noProductMessage = true
   } else {
    state.productListInStore = response.data.products
    state.totalPages = response.data.pagination.totalPages
    state.totalProducts = response.data['pagination']['totalEntries']
    state.loader = false
    state.smthWemtWrongMessage = false
    state.noProductMessage = false
   }


  },
  setAspectValuesToState(state, payload){

    if(payload.addAspect == true){
      let added = false
      state.aspectValuesList.forEach(function (item){
        if(item.name == payload.aspectName){
          item.values.push(payload.aspectValue)
          return added = true
        }
      })
      if(!added){
        let selected = {
          "name": payload.aspectName,
          "values":[payload.aspectValue]
        }
        state.aspectValuesList.push(selected)
      }
    } else if(payload.addAspect == false)  {
      state.aspectValuesList.forEach(function (item){
        if(item.name == payload.aspectName){
          item.values.forEach(function (value){
            if(value == payload.aspectValue){
              var index = item.values.indexOf(value);
              if (index > -1) {
                item.values.splice(index, 1);
              }
              return item.values
            }
          })
          if(item.values.length == 0){
            var itemIndex = state.aspectValuesList.indexOf(item);
            if (itemIndex > -1) {
              state.aspectValuesList.splice(itemIndex, 1);
            }
          }
        }
      })
    }

  },

  setSortedProductListToState(state, resp){
   if(resp){
    state.productListInStore = resp.data.products
    state.totalPages = resp.data.pagination.totalPages
    state.totalProducts = resp.data['pagination']['totalEntries']
    state.loader = false
   }
  },
}



const actions = {
  async searchProduct({commit, state}, payload){
    let url = payload
    let params = new URLSearchParams(url.search.slice(1));
    let keyword = params.get("k")
    let response = ''
    commit('waiting')
    try {
      for(let i = 0; i < 3; i++){
        response = await ProductList.searchProducts(payload)
        if(response && response.status == 200){ break;}
      }      
      if(response.data.total_products.count == 0){
        commit('noProductMessage')
      }else if(response.data.total_products.count > 0){
        commit('newProductList', response)
        if(state.keyword != keyword){
          commit('selectedAspects')
          commit('keywordUpdate', keyword)
          commit('aspects')
          let aspects = await ProductList.aspectList(keyword)
          if(aspects && aspects.data.aspectHistogramContainer){
            commit('aspects',aspects.data.aspectHistogramContainer.aspect)
          }
        }
      }
    } catch (e) {
      commit('smthWemtWrongMessage')
      commit('selectedAspects')
      commit('setProductListLoader', false)
      console.log("SEARCH PRODUCT HAS ERROR",e)
    }
  },
  async updateSearchKeyword({commit}, payload){
   try {
     let response = await ProductList.fetchProductList(payload, 50, 1)
     if(response){
      commit('setProductLisToState', response)
     }
   } catch (e) {
    console.log(e)
   }
  },
  selectedAspects({commit}){
    commit('selectedAspects')
  },

  async updateProductList({commit}, payload){

    try {
      let response = await ProductList.fetchProductList(
        payload.skeyword,
        payload.productPerPage,
        payload.pageNum
      )
     if(response){
      commit('setProductLisToState', response)
     } else {
      for(let i = 0; i < 3; i++){
       response = await ProductList.fetchProductList(
        payload.skeyword,
        payload.productPerPage,
        payload.pageNum
       )
       if(response){
        if(response.data.products.length === 0){
         commit('setProductLisToState', response)
        } else {
         commit('setProductLisToState', response)
        }
        break
       }
      }
      if(response){
       if(response.data.products.length === 0){
        commit('setProductLisToState', response)
       } else {
        commit('setProductLisToState', response)
       }
      } else {
       commit('setProductLisToState', response)
      }
     }
    } catch {
      console.log("Error in actions")
    }
  },

  async rootCategoryList({commit}, payload){

    let response = await ProductList.fetchRootCategoryList(payload.skeyword,
                                                           payload.productPerPage,
                                                           payload.pageNum)

    if(response){
     let respObj = {
      "response": response,
      "updateProductList": true
     }
     commit('setRootCategoryListToState', respObj)
    }

  },

  async childCategoryList({commit}, payload){

    let response = await ProductList.fetchChildCategory(payload.skeyword,
                                                        payload.productPerPage,
                                                        payload.pageNum,
                                                        payload.catID,)

   if(response){
    let catObj = {
     'skeyword': payload.skeyword,
     'response': response,
     "catID":payload.catID,
     "categoryName": payload.categoryName,
     "level": payload.level
    }
    commit('setChildCategoryToState', catObj)
   } else {
    for(let i = 0; i < 3; i++){
     response = await ProductList.fetchProductList(
      payload.skeyword,
      payload.productPerPage,
      payload.pageNum
     )
     if(response){
      if(response.data.products.length === 0){
       commit('setChildCategoryToState', catObj)
      } else {
       let catObj = {
        'skeyword': payload.skeyword,
        'response': response,
        "catID":payload.catID,
        "categoryName": payload.categoryName,
        "level": payload.level
       }
       commit('setChildCategoryToState', catObj)
      }
      break
     }
    }
    if(response){
     if(response.data.products.length === 0){
      commit('setChildCategoryToState', catObj)
     } else {
      let catObj = {
       'skeyword': payload.skeyword,
       'response': response,
       "catID":payload.catID,
       "categoryName": payload.categoryName,
       "level": payload.level
      }
      commit('setChildCategoryToState', catObj)
     }
    } else {
     commit('setChildCategoryToState', catObj)
    }
   }

  },
  async showChildCategoryList({commit}, payload){
    commit('setShowCategoryToState', payload)
  },


  async updateAspectList({commit}, payload){
    try {
      let respAspect = await ProductList.fetchAspectList(
        payload.skeyword,
        payload.productPerPage,
        payload.pageNum
      )
     if(respAspect){
      commit('setAspectListToState', respAspect)
     }

    } catch {
      console.log("Error in actions")
    }
  },

  async filterProductListByAspects({commit}, payload){
    try {
      let response = await ProductList.fetchProductListByAspects(
        payload.skeyword,
        payload.productsPerPage,
        payload.pageNum,
        state.aspectValuesList
      )
     if(response){
      commit('setProductListByAspectToState', response)
     } else {
      for(let i = 0; i < 3; i++){
       response = await ProductList.fetchProductList(
        payload.skeyword,
        payload.productPerPage,
        payload.pageNum
       )
       if(response){
        if(response.data.products.length === 0){
         commit('setProductListByAspectToState', response)
        } else {
         commit('setProductListByAspectToState', response)
        }
        break
       }
      }
      if(response){
       if(response.data.products.length === 0){
        commit('setProductListByAspectToState', response)
       } else {
        commit('setProductListByAspectToState', response)
       }
      } else {
       commit('setProductListByAspectToState', response)
      }
     }
    } catch {
      console.log("Error in filter by aspect ")
    }
  },

  async sortProductList({commit}, payload){
   try {
    let response = await ProductList.fetchSortedProductList(payload.skeyword,
     payload.productPerPage,
     payload.pageNum,
     payload.sortType,
     payload.minPrice,
     payload.maxPrice
    )

    if(response){
     commit('setSortedProductListToState', reponse)
    } else {
     for(let i = 0; i < 3; i++){
      response = await ProductList.fetchProductList(
       payload.skeyword,
       payload.productPerPage,
       payload.pageNum
      )
      if(response){
       if(response.data.products.length === 0){
        this.$router.push('no-product')
       } else {
        commit('setSortedProductListToState', reponse)
       }
       break
      }
     }
     if(response){
      if(response.data.products.length === 0){
       this.$router.push('no-product')
      } else {
       commit('setSortedProductListToState', reponse)
      }
     } else {
      this.$router.push('smth-went-wrong')
     }
    }

   } catch (e) {
    throw e
   }
  },
  async updateCategory({commit}, payload){
    try {
      let response = await ProductList.fetchRootCategoryList(payload.skeyword,
                                                             payload.productPerPage,
                                                             payload.pageNum)
     if(response){
      let respObj = {
       "response": response,
       "updateProductList": true
      }
      commit('setRootCategoryListToState', respObj)
     }
    } catch (e) {
     throw e
    }
  },
  updateAspectValuesList({commit}, payload){
    commit('setAspectValuesToState', payload)
  },



}
const getters = {
  categoryTrees: (state) =>{
      return state.categoryTrees
  }

}

export default {
  namespaced:true,
  state,
  mutations,
  actions,
  getters
}
