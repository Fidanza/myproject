export { default as AppHeader } from './app/shared/components/app-header/app-header.vue';
export { default as AppPage404 } from './app/shared/components/error-pages/page-404.vue';