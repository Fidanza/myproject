import { productsData } from './shared/state';

export default {
  modules: {
    productsData
  }
};