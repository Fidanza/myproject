import accountRoutes from '../app/accounts/account-routes'

const routes = [{
	path: '/home',
	component: () =>
		import('layouts/layout.vue'),

	children: [{
		name: 'test',
		path: '/test',
		component: () => import('pages/test.vue'),
	}, {
		path: '/',
		component: () =>
			import('src/app/products/products-home.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: false
		}
	},

	{
		path: '/test',
		component: () =>
			import('src/app/billing/checkout/sendContactForm.vue')
	},
  {
    path: '/order-details',
    name:'order-details',
    component: () =>
      import ('src/app/accounts/order-details.vue'),
    meta: {
      public: false,
      onlyWhenLoggedOut: false
    }
  },
	{
		path: '/cart',
		component: () =>
			import('src/app/billing/carts/cart.vue'),
		meta: {
			public: true
		}
	},
	{
		path: '/activation',
		component: () =>
			import('src/app/accounts/activation.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: true
		}
	},
	{
		path: '/reset-password',
		component: () =>
			import('src/app/accounts/reset-password.vue'),
		meta: {
			public: false,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/your-account',
		component: () =>
			import('src/app/accounts/your-account.vue'),
		meta: {
			public: false,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/login-security',
		component: () =>
			import('src/app/accounts/login-security.vue'),
		meta: {
			public: false,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/your-orders',
		component: () =>
			import('src/app/accounts/your-orders.vue'),
		meta: {
			public: false,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/order-confirmation',
		component: () =>
			import('src/app/billing/order-confirmation.vue'),
		meta: {
			public: false,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/your-address-book',
		component: () =>
			import('src/app/accounts/your-address-book.vue'),
		meta: {
			public: false,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/payment-methods',
		component: () =>
			import('src/app/accounts/payment-methods.vue'),
		meta: {
			public: false,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/wishlist',
		component: () =>
			import('src/app/products/wishlist/wishlist.vue'),
	},
	{
		path: '/mobile-cart',
		component: () =>
			import('src/app/billing/carts/mobile-cart-full.vue')
	},
	{
		path: 'product-list',
		name: 'search',
		component: () =>
			import('src/app/products/product-list/product-list.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: false
		},
	},
	{
		path: 'product-list/:kw/:pn',
		name: 'searchByKW',
		component: () =>
			import('src/app/products/product-list/product-list.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: false
		},
	},
	{
		path: 'product-details',
		name: 'details',
		component: () =>
			import('src/app/products/product-details/product-details.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/category-list',
		name: 'category-list',
		component: () =>
			import('src/app/products/categories/category-list.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/review/:id',
		name:'review',
		component: () =>
			import('src/app/products/review/review.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: false
		},
	}, {
		path: '/support',
		name: 'support',
		component: () =>
			import('src/app/accounts/support/support.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/navbar',
		component: () =>
			import('src/app/shared/navbar/navbar.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: false
		}
	},
	...accountRoutes,

	{

		path: '/error',
		name: 'error',
		component: () =>
			import('src/app/extra-pages/page-404.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: false
		}
	},
	{
		path: '/no-product',
		name: 'no-product',
		component: () =>
			import('src/app/extra-pages/no-product.vue'),
		meta: {
			public: true,
			onlyWhenLoggedOut: false
		}
	},
	{

        path: '/smth-went-wrong',
        name: 'smth-went-wrong',
        component: () =>
          import ('src/app/extra-pages/smth-went-wrong.vue'),
        meta: {
          public: true,
          onlyWhenLoggedOut: false
        }
      },
      {
        path: '/trending',
        name : "trending",
        component: () =>
          import ('src/app/extra-pages/trending.vue'),
        meta: {
          public: true,
          onlyWhenLoggedOut: false
        }
      },
      {
        path: '/bestsellers',
        component: () =>
          import ('src/app/extra-pages/bestsellers.vue'),
        meta: {
          public: true,
          onlyWhenLoggedOut: false
        }
      },
      {
        path: '/todays-deals',
        component: () =>
          import ('src/app/extra-pages/todays-deals.vue'),
        meta: {
          public: true,
          onlyWhenLoggedOut: false
        }
      },
      {
        path: '/successful-register',
        component: () =>
          import ('src/app/extra-pages/successful-register.vue'),
        meta: {
          public: true,
          onlyWhenLoggedOut: true
        }
      },
      {
        path: '/faqs',
        component: () =>
          import ('src/app/extra-pages/frequently-asked questions.vue'),
        meta: {
          public: true,
        }
      },
      {
        path: '/notify',
        component: () =>
          import ('src/app/extra-pages/user-messages.vue'),
        meta: {
          public: true,
          onlyWhenLoggedOut: true
        }
      },
    ]
  },

{
	path: '/',
	component: () =>
		import('layouts/checkout-layout.vue'),
	children: [
		{
			path: '/payment-checkout',
			component: () => import('src/app/billing/checkout/checkout-main.vue'),
			meta: {
				public: false,
				onlyWhenLoggedOut: false
			}
		},
		{
			path: '/',
			component: () =>
				import('layouts/comming.vue')
		},
	]
},

]


// Always leave this as last one
if (process.env.MODE !== 'ssr') {
	routes.push({
		path: '',
		component: () =>
			import('layouts/checkout-layout.vue'),
		children: [{
			path: '*',
			component: () =>
				import('src/app/extra-pages/page-404.vue')
		}]
	})
}
export default routes
